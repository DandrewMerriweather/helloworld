/**
 * Created by drewmerriweather on 9/5/17.
 */




//component classes
class warningKlaxon {

    constructor(selector){

        //counter
        this.count = 0;

        // save a reference to this component's root element
        this.el = document.querySelector(selector);


        this.dangerZone = this.el.querySelector('.dangerZone');
        this.feedback = this.el.querySelector('.feedback');
        //Listen to mouseover ont the danger zone
        //run the danger entered method if the mouse does enter..
        this.dangerZone.addEventListener('mouseOver', this.dangerEntered.bind(this));
        this.dangerZone.addEventListener('mouseOut', this.dangerLeft.bind(this));
    }



        dangerEntered (){
            this.count ++;
            this.feedback.innerHTML = "you have enter the danger zone " + this.count + "time. Leave quickly";
    }

        dangerLeft(){
            this.feedback.innerHTML ="";
    }
}

