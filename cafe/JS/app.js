/**
 * Created by drewmerriweather on 9/27/17.
 */



Vue.component("customer-template",{
   template:"#c-template",
    data:function (){
       return{
           order_type:"",
       }
    },
    computed:{
        showText2 (){
            if(this.order_type === '') return '';
            return 'waiting for' + this.order_type
        }

    }
});




Vue.component("barista-template",{
    template: "#b-template",
    data: function () {
        return{
            order_type:"",
            order_value: "",
        }
    },
    computed: {
        showText () {
            if(this.order_type === '') return '';
            return 'One ' + this.order_type + ' that would be ' + this.order_value
        }
    },
    methods: {
        choose: function (order_type) {
            this.order_type = order_type;

            if (this.order_type == "drip") {
                this.order_value = "$10";
            }
            if (this.order_type == "frenchpress") {
                this.order_value = "$20";
            }
            if (this.order_type == "aeropress") {
                this.order_value = "$30";
            }

        }
    },


});



new Vue ({
    el:"#app",
    data:function () {
        return{
            showing:true
        }
    }
});