/**
 * Created by drewmerriweaher on 9/21/17.
 */
new Vue({
    el: '#app',
    data: {
        currentComponent: null,
        componentsArray: ['drip', 'french press', 'Aeropress']
    },
    components: {
        'drip': {
            template: "<div><h1>drip component</h1> <h1 class='drip'>drip component</h1><div>lorem</div> </div>",
        },
        'french press': {
            template: "<h1>french press component</h1>",
            template: "<h1 class=\"drinkTwo\">french press component</h1>"
        },
        'Aeropress': {
          template: '<h1>Aeropress component</h1>',
          template: '<h1 class="drinkThree">drinkThree component</h1>'
        }
},
methods: {
    swapComponent: function(component)
    {
        setTimeout(() => {
            console.log('brewing')
        }, 1500);
        this.currentComponent = component;
    }
}
});
