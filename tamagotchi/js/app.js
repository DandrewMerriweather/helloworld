/**
 * Created by drewmerriweather on 8/31/17.
 */

class Tamagotchi {

    constructor(name, type, energy, selector) {
        this.name = name;
        this.type = type;
        this.energy = energy;
        this.element = document.querySelector(selector);

        //start the tamgochi simulation

        setInterval(this.update.bind(this), 1000);



    }
    //exposes some debug data
    report () {
        console.log(this.name + " is a " + this.type + " and has e " + this.energy);
    }

    eat (){
    //add energy to Tamagotchi
        this.energy += 5;
    }


        //common video game method
    //runs the Tamagotchi 'sim'
    update() {
        if (this.energy){
            console.log(this.energy);
        }
        this.energy--;
        this.element.innerHTML = this.name + ' has ' + this.energy + ' energy';


        if (this.energy === 0) {
            alert("Your  tamagotchi ran  away :(");

        }
    }
}

    //make and instance of a Tamagotchi
    var myTamagotchi = new Tamagotchi('Druid', 'heart', 24, "#dvInfo" );
    myTamagotchi.report(); //have






